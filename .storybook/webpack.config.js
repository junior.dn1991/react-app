const override = require('../config-overrides.js');

module.exports = (baseConfig, env, defaultConfig) => {
  return override(defaultConfig, env);
};
