const path = require('path');

const { injectBabelPlugin } = require('react-app-rewired'); // eslint-disable-line import/no-extraneous-dependencies

const {
  createLoaderMatcher,
  createRuleMatcher,
  findRule,
} = require('./src/utils/rewriteUtils');

// eslint-disable-next-line no-undef
const rootDir = path.resolve(__dirname);
const srcDir = path.resolve(rootDir, 'src/');

const cssRuleMatcher = createRuleMatcher(/\.css$/);
const cssLoaderMatcher = createLoaderMatcher('css-loader');

module.exports = function override(config, env) {
  // adding source modules begin
  config.resolve.modules.push(srcDir);
  // adding source modules end

  // adding ant design babel import begin
  config = injectBabelPlugin(['import', { libraryName: 'antd', libraryDirectory: 'es', style: true }], config); // eslint-disable-line no-param-reassign
  // adding ant design babel import end

  // css setup begin
  const cssModulesRule = findRule(config.module.rules, cssRuleMatcher);
  cssModulesRule.exclude = /node_modules/;
  const cssModulesRuleCssLoader = findRule(cssModulesRule, cssLoaderMatcher);
  cssModulesRuleCssLoader.options = Object.assign({ modules: true, camelCase: true, localIdentName: '[local]___[hash:base64:5]' }, cssModulesRuleCssLoader.options);

  const lessRules = {
    test: /\.less$/,
    use: [
      'style-loader',
      'css-loader',
      { loader: 'less-loader', options: { javascriptEnabled: true } },
    ],
  };

  const oneOfRule = config.module.rules.find(rule => rule.oneOf !== undefined);
  if (oneOfRule) {
    oneOfRule.oneOf.unshift(lessRules);
  } else {
    config.module.rules.push(lessRules);
  }
  // css setup end

  return config;
};
