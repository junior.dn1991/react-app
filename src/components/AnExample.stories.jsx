import React from 'react';
import { storiesOf } from '@storybook/react';
import AnExample from './AnExample';

storiesOf('AnExample', module)
  .add('with default text', () => (
    <AnExample />
  ))
  .add('with custom text', () => (
    <AnExample text="Custom text" />
  ));
