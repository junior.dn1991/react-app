import React from 'react';
import PropTypes from 'prop-types';

const PageLoader = ({ isLoading, error }) => {
  if (isLoading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Sorry, there was a problem loading the page.</div>;
  }

  return null;
};

PageLoader.propTypes = {
  isLoading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.bool, PropTypes.shape({})]),
};

PageLoader.defaultProps = {
  isLoading: false,
  error: false,
};

export default PageLoader;
