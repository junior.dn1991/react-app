import React from 'react';
import { storiesOf } from '@storybook/react';
import PageLoader from './PageLoader';

storiesOf('PageLoader', module)
  .add('default', () => <PageLoader />)
  .add('loading', () => <PageLoader isLoading />)
  .add('with error', () => <PageLoader error={{ message: 'error' }} />);

