import React from 'react';
import PropTypes from 'prop-types';

import styles from './AnExample.css';

const AnExample = ({ text }) => <div className={styles.anExample}>{text}</div>;

AnExample.propTypes = {
  text: PropTypes.string,
};

AnExample.defaultProps = {
  text: 'This is an example',
};

export default AnExample;
