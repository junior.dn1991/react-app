This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

**Additional packages**
  1. husky / commitlint for Hooks
  2. prop-types
  3. storybook (Full control webpack mode with rewired webpack configs)
  4. eslint
  5. redux with redux-thunk middleware and redux-shelf library
  6. react-app-rewire
  7. Enzyme
  8. storyshots
  9. axios
  10. store (memory, local, session and cookie storages)
  11. React Router
  12. React Loadable

**Custom Webpack configuration:**  
  1. Enabled CSS Modules with camel case mode.

**Next steps**
  1. WIP
